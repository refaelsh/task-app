import React from "react";
import Overview from "./components/Overview.js";
import uniqid from "uniqid";

export default class wApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newTask: {
        text: ``,
        id: uniqid(),
      },
      taskArray: [
        { text: `Some default task #1`, id: uniqid() },
        { text: `Some default task #2`, id: uniqid() },
      ],
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      newTask: { text: event.target.value, id: this.state.newTask.id },
    });
  }

  handleSubmit(event) {
    this.setState((state, props) => ({
      newTask: { text: ``, id: uniqid() },
      taskArray: this.state.taskArray.concat(this.state.newTask),
    }));

    event.preventDefault();
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Task text:
            <input
              type="text"
              value={this.state.newTask.text}
              onChange={this.handleChange}
              name="taskText"
            />
          </label>
          <input type="submit" value="Add" />
        </form>

        <Overview taskArray={this.state.taskArray} />
      </div>
    );
  }
}
