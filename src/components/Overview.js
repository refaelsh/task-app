import React from "react";

export default class Overview extends React.Component {
  render() {
    const res = this.props.taskArray.map((task) => (
      <div key={task.id}>{task.text}</div>
    ));

    return res;
  }
}
